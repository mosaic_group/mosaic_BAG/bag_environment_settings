#! /bin/bash

# ---------------------------------------------------------------------------------------------------
#   This script contains BASH utility functions
#       - for example for tests in sourceme.sh
#
#   Usage:
#       - source BAG2_environment_settings
#       - then call the required functions
#
#   Originally added 2023 by <martin.koehler@silicon-austria.com>
# ---------------------------------------------------------------------------------------------------


# ---------------------------------------------------------------------------------------------------
# Functions for Terminal colors
# ---------------------------------------------------------------------------------------------------

function color_set_red() {
    local RED_COLOR="\033[91m"
    echo -e $RED_COLOR
}

function color_set_yellow() {
    local YELLOW_COLOR="\033[93m"
    echo -e $YELLOW_COLOR
}

function color_set_green() {
    local GREEN_COLOR="\033[32m"
    echo -e $GREEN_COLOR
}

function color_reset() {
    local RESET_COLOR="\033[0m"
    echo -e $RESET_COLOR
}

# ---------------------------------------------------------------------------------------------------
# Logging functions
# ---------------------------------------------------------------------------------------------------

function log_error() {
    color_set_red
    echo $*
    color_reset
}

function log_warning() {
    color_set_yellow
    echo $*
    color_reset
}

function log_success() {
    color_set_green
    echo $*
    color_reset
}

# ---------------------------------------------------------------------------------------------------
# Python checking functions
# ---------------------------------------------------------------------------------------------------

function is_venv() {

/usr/bin/env python3 << END
import sys
is_venv = (hasattr(sys, 'real_prefix') or (hasattr(sys, 'base_prefix') \
          and sys.base_prefix != sys.prefix))
exit_code = 0 if is_venv else 1
sys.exit(exit_code)
END

}

# ---------------------------------------------------------------------------------------------------
# Prerequisite checking functions
# ---------------------------------------------------------------------------------------------------

function check_tool_available() {
    # This function checks if the required version of tools is available.
    #
    # Usage examples:
    #                       TOOL     VERSION COMMAND           >VERSION           VERSION_BLACKLIST_REGEX
    # ---------------------------------------------------------------------------------------------------
    # check_tool_available  klayout  "klayout -v"              "KLayout 0.28.7"   ""
    # check_tool_available  xschem   "xschem -v | head -n 1"   "XSCHEM V2.9.9"    "XSCHEM V3\\.4\\.1"

    local TOOL="$1"                          # f.ex. "klayout"
    local VERSION_TEST_CMD_LINE="$2"         # f.ex. "klayout -v" or "klayout --version" or "klayout -V"
    local VERSION_MUST_BE_GREATER_THAN="$3"  # f.ex. "KLayout 0.28.7", which means, we need >0.28.7
    local VERSION_BLACKLIST_REGEX="$4"       # regex matching blacklisted versions (i.e. xschem 3.4.1 has a bug)

    if ! (which "$TOOL" > /dev/null)
    then
        log_error "ERROR: required tool '$TOOL' is not in '\$PATH' (version must be > '${VERSION_MUST_BE_GREATER_THAN}')"
        return 1
    fi

    local INSTALLED_VERSION=$(eval $VERSION_TEST_CMD_LINE)
    local CMP_RESULT=$(echo -e "${INSTALLED_VERSION}\n${VERSION_MUST_BE_GREATER_THAN}" | sort -V | uniq | tail -n 1)
    if [ "$CMP_RESULT" = "$VERSION_MUST_BE_GREATER_THAN" ]
    then
        log_error "ERROR: '$TOOL' has version $INSTALLED_VERSION (but version must be > '${VERSION_MUST_BE_GREATER_THAN}')"
        return 1
    fi

    if [ -n "$VERSION_BLACKLIST_REGEX" ]
    then
        if echo $INSTALLED_VERSION | grep -e "$VERSION_BLACKLIST_REGEX"
        then
            log_error "ERROR: '$TOOL' has version $INSTALLED_VERSION which is blacklisted (i.e. due to bugs)"
            return 1
        fi
    fi

    echo "Version check succeeded: $TOOL ($INSTALLED_VERSION)"
}

