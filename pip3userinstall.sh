#! /usr/bin/env bash

# -----------------------------------------------------------------------
# NOTE: add additional required modules here

PIP_MODULES_TO_INSTALL=" \
    PyYaml \
    numpy \
    scipy \
    h5py \
    zmq \
    shapely \
    rtree \
    future \
    jinja2 \
    IPython \
    openmdao \
    sphinx_rtd_theme \
    GitPython \
    python-gitlab \
    forallpeople \
    klayout \
    gdsfactory \
    volare \
    pyspice \
"

# -----------------------------------------------------------------------

DIR=$(dirname -- $(realpath ${BASH_SOURCE}))

source ${DIR}/bash-utils.sh   # functions {is_venv, log_warning}

if is_venv; then
    PIP_ARGS=""  # venv does not work with user
else
    PIP_ARGS="--user"
fi

for module in ${PIP_MODULES_TO_INSTALL}; do
    CMD="pip3 install $PIP_ARGS $module"
    log_warning Running: $CMD
    $CMD
done

exit 0

